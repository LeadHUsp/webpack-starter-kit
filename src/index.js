// Test import of a JavaScript module
// import { initReactApp } from '@/js/react-example';
// import { initVueApp } from '@/js/vue-example';
import { Drawer } from '@/blocks/drawer';
import { Modal } from '@/blocks/modal/modal';
// Test import of styles
import '@/styles/index.scss';

// import libs
import 'swiper/scss';

// Appending to the DOM

// initReactApp();
// initVueApp();
// if (document.querySelector('header')) {
//   import(/* webpackChunkName: "header" */ '@/blocks/header/header').then((header) =>
//     header.default()
//   );
// }
class Root {
  constructor() {
    document.addEventListener('DOMContentLoaded', () => {});
  }
  initApp = () => {
    new Drawer();
    window.modal = new Modal();
  };
  afterInitApp = () => {};
}
new Root();
